from tkinter import *
from tkinter import ttk, font, filedialog, messagebox
import tkinter as tk
import moduleChequeTransf as rbotChequeT
import moduleSuper as rbotSuper
import hospital_info as rrr

import os

# Gestor de geometría (pack)
class Aplicacion():
    def __init__(self):
        self.raiz = Tk()


        # Define el Titulo de la Ventana de la Aplicacion y no permite el cambio
        # del tamaño del frame
        self.imagenLogo = tk.PhotoImage(file="./iconos/logo.png")
        self.image2 = tk.Image("photo", file="./iconos/rhani.png")
        self.raiz.tk.call('wm','iconphoto',self.raiz._w,self.image2)
        self.raiz.title("maui-B")
        self.raiz.resizable(width=False, height=False)

        # Fuente
        fuente = font.Font(weight='bold')

        # Define el Titulo de la aplicacion
        self.titulo = ttk.Label(self.raiz, image=self.imagenLogo, text="",
                               font=fuente)

        # Declaraccion texto version
        # Tambien defino lo dos separadores
        self.ver = ttk.Label(self.raiz, text="   innovacion-R | v. 0.1   ")
        self.barra1 = ttk.Label(self.raiz,text="",
                               font=fuente,background="indian red",compound="left")
        
        self.separ1 = ttk.Separator(self.raiz, orient=HORIZONTAL)
        self.separ2 = ttk.Separator(self.raiz, orient=HORIZONTAL)
        self.separ3 = ttk.Separator(self.raiz, orient=HORIZONTAL)
        self.separ4 = ttk.Separator(self.raiz, orient=HORIZONTAL)

        # IMAGENES
        self.imagenInten = tk.PhotoImage(file="./iconos/intendenc.png")
        self.imagenImpactar = tk.PhotoImage(file="./iconos/impactar.png")
        self.imagenTransferencia = tk.PhotoImage(file="./iconos/jul.png")
        self.imageClose = tk.PhotoImage(file="./iconos/close2.png")
        
        # BOTONES
        
        self.botonBajarArchivo = ttk.Button(self.raiz,width=40, text="Bajar Archivos",image=self.imagenInten,compound="left",
                                 command=self.bajarArchivo)
##        
        self.botonImpactar = ttk.Button(self.raiz,width=40, text="Impactar MHO",image=self.imagenImpactar,compound="left",
                                command=self.impactarMHO)
        
        self.botonTransferencia = ttk.Button(self.raiz,width=40, text="Impactar Transferencias/Cheques",image=self.imagenTransferencia,compound="left",
                                 command=self.transferenciaMHO)
    
        self.salir = ttk.Button(self.raiz, text="Salir",image=self.imageClose,compound="left",
                                 command=quit)


        #################################
        ####### PACKING - PACKING #######
        ####### PACKING - PACKING #######
        #################################
        self.barra1.pack(side=TOP, fill=BOTH, expand=True,
                         padx=0, pady=0)


        self.titulo.pack(side=TOP, fill=BOTH, expand=True,
                        padx=100, pady=10)

        self.botonBajarArchivo.pack(side=TOP,expand=False,
                         padx=100, pady=10)
        self.botonImpactar.pack(side=TOP,expand=False,
                        padx=100, pady=10)
        
        self.botonTransferencia.pack(side=TOP,expand=False,
                         padx=100, pady=10)
        
        
        self.separ3.pack(side=TOP, fill=NONE, expand=True,
                         padx=5, pady=5)

        self.salir.pack(side=RIGHT, fill=None, expand=False,
                         padx=5, pady=5)

        self.raiz.mainloop()


    def impactarMHO(self):
        os.system("cls")

        rutaArchivo = filedialog.askopenfilename(initialdir = "./",title = "Select file",filetypes = (("Archivos csv","*.csv"),))        
        if rutaArchivo == '':
            messagebox.showinfo("Info:", "No seleccionaste ningun archivo.")
        else:
            datosExp = []
            try:
                resp = str(rbotSuper.abrirArchivoCSV(rutaArchivo))
            except Exception as e:
                input(str(e))


    def transferenciaMHO(self):
        os.system("cls")

        rutaArchivo = filedialog.askopenfilename(initialdir = "./",title = "Select file",filetypes = (("Archivos csv","*.csv"),))        
        if rutaArchivo == '':
            messagebox.showinfo("Info:", "No seleccionaste ningun archivo.")
        else:
            datosExp = []
            try:
                resp = str(rbotChequeT.abrirArchivoCSV(rutaArchivo))
            except Exception as e:
                input(str(e))


    def bajarArchivo(self):
        os.system("cls")
        def helpCSV():
            print("AYUDA : El archivo tiene que estar de la forma. (sin encabezados)")
            print("▬"*41)
            print("▓  Expediente |  Fecha  |  Movimiento   ▓")
            print("▬"*41)
        
        helpCSV()
        rrr.main()

    
    def sel():
        selection = "You selected the option " + str(var.get())
        label.config(text = selection)

def main():
    mi_app = Aplicacion()
    return 0

if __name__ == '__main__':
    main()
