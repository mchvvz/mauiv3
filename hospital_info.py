#!/usr/bin/python3

#from PyQt4.QtCore import pyqtSlot
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtCore import pyqtSlot
import sys
import sss

#---------------------------------------------------------------------------------------
    
def main():
    print("Iniciando Proceso...")

#---------------------------------------------------------------------------------------
    
#APPLICATION    
    app = QtWidgets.QApplication(sys.argv)

#VENTANA PRINCIPAL
    mainform = QtWidgets.QDialog()
    mainform.setFixedSize(655, 400)
    mainform.move(150,100)
    mainform.setWindowTitle('Consulta Info SSS')

#CALENDARS   
    desde = QtWidgets.QCalendarWidget(mainform)
    desde.move(20, 90)
    hasta = QtWidgets.QCalendarWidget(mainform)
    hasta.move(330, 90)
    #textbox.resize(280,40)

#LABELS
    info_label = QtWidgets.QLabel('Seleccione las fechas para su busqueda:', mainform)
    info_label.move(20, 20)
    info_label.resize(300,30)
    
    desde_label = QtWidgets.QLabel('Desde', mainform)
    desde_label.move(155, 60)
    desde_label.resize(300,30)
    
    hasta_label = QtWidgets.QLabel('Hasta', mainform)
    hasta_label.move(465, 60)
    hasta_label.resize(300,30)
    
    prog_label = QtWidgets.QLabel('', mainform)
    prog_label.move(25, 360)
    prog_label.resize(450,30)


#BARRA DE PROGRESO   
    #bar = QtWidgets.QProgressBar(mainform)
    #bar.resize(320,20)    
    #bar.setValue(0)
    #bar.move(20,400)


#BOTON START
    start_btn = QtWidgets.QPushButton('Comenzar Busqueda', mainform)
    start_btn.resize(start_btn.sizeHint())
    start_btn.move(250, 300)

#FUNCION BOTON START
    def start():
        dateDesde = desde.selectedDate()
        dateHasta = hasta.selectedDate()
        if (dateHasta < dateDesde):
            prog_label.setText("ERROR: La fecha 'Hasta' no puede ser menor que la fecha 'Desde'")
        else:
            prog_label.setText("")
            strDesde = dateDesde.toString("dd/MM/yyyy")
            strHasta = dateHasta.toString("dd/MM/yyyy")
            sss.begin(strDesde, strHasta)
            print("Proceso Finalizado.")
            mainform.close()
  

#MOSTRAR LA VENTANA
    mainform.show()

#CLICKS BOTONES
    start_btn.clicked.connect(start)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
