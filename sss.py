#!/usr/bin/python3
import os
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import pandas as pd
import numpy as np

######################################
## Set username, password and login ##

def doLogin(driver, comarbUsername, comarbPassword):
    usernameElement = driver.execute_script('\
        var userbox = document.querySelectorAll("input")[1];\
        return userbox;\
        ')
    passwordElement = driver.execute_script('\
        var passbox = document.querySelectorAll("input")[2];\
        return passbox;\
        ')
    buttonElement = driver.find_element_by_class_name("botong")

    usernameElement.send_keys(comarbUsername)
    passwordElement.send_keys(comarbPassword)
    buttonElement.click()

######################################

def searchHospitalDetails(driver, HPGD, strDesde, strHasta):
    
    HPGDbox = driver.execute_script('\
        var codBox = document.querySelectorAll("input")[2];\
        return codBox;\
        ')
    
    searchButton = driver.execute_script('\
        var buscar = document.querySelectorAll("input")[4];\
        return buscar;\
        ')
    
    HPGDbox.send_keys(HPGD)
    searchButton.click()
    
    hospitalLink = driver.execute_script('\
        var link = document.querySelectorAll("td.HeaderTabla2 a")[0].href;\
        return link;\
        ')
    
    driver.get(hospitalLink)
    
    dateFromElement = driver.find_element_by_id("exampleInputName2")
    dateToElement = driver.find_element_by_id("exampleInputEmail2")
    filterButton = driver.execute_script('\
        var filtrar = document.getElementsByClassName("btn btn-default")[0];\
        return filtrar;\
        ')
    
    #currentDate = time.strftime("%x")
    #currenDateFormatted = currentDate.split("/")[1] + "/" + currentDate.split("/")[0] + "/20" + currentDate.split("/")[2]
    #currenDateFormatted1 = "20/02/2018"
    #currenDateFormatted2 = "22/02/2018"
    
    dateFromElement.send_keys(strDesde)
    dateToElement.send_keys(strHasta)
    
    filterButton.click()
    
    table = driver.execute_script('\
        var tabla = document.getElementsByTagName("tbody")[0].children;\
        var entries = tabla.length;\
        var tableList = [];\
        var last9CellsRow = 0;\
        if (entries > 2){\
        for (var i = 1; i < tabla.length - 1; i++){\
        row = tabla[i];\
        cells = row.childNodes;\
        var rowList = [];\
        var cantcells = cells.length;\
        if (cantcells == 9) {\
        last9CellsRow = i};\
        for (var j = 0; j < cantcells; j++){\
        cell = cells[j];\
        span = cell.getAttribute("rowspan");\
        rowList.push([cell.innerText,span]);}\
        if (cantcells == 7){\
        rowList.push(["","1"]);\
        rowList.push(["","1"]);}\
        tableList.push(rowList);}\
        }\
        return tableList;\
        ')
    
    return table

######################################

def normalizeExpediente(numExp):
    if "(" in numExp:
        numExp = numExp.split("(")[0][:-1]
    
    if "/" in numExp:
        numExp = numExp.split("/")[0] + "/" + numExp.split("/")[1][-2:]
    
    if "-" in numExp:
        numExp = numExp.split("-")[1] + "/" + numExp.split("-")[0][2:]

    return numExp

######################################

def normalizeFactura(numFactura):
    #index = 0
    #for num in numFactura:
    #    if (num == "0"):
    #        index = index + 1
    #    else:
    #        break
    #numFactura = numFactura[index:]
    if (len(numFactura) > 8):
        numFactura = numFactura[-8:]
    elif (len(numFactura) < 8):
        rest = 8 - len(numFactura)
        ceros = "0"*rest
        numFactura = ceros + numFactura
    return numFactura

######################################

def verifyAmount(impAprobado, impDepositado):
    
    if (isinstance(impAprobado, str)):
        impAprobado = impAprobado.split("$")[1]
        impAprobado = impAprobado.replace(".", "")
        impAprobado = impAprobado.replace(",", ".")
    
    if (isinstance(impDepositado, str)):
        impDepositado = impDepositado.split("$")[1]
        impDepositado = impDepositado.replace(".", "")
        impDepositado = impDepositado.replace(",", ".")
    
    res = float(impAprobado) - float(impDepositado)
    
    obs = ""
    
    if res > 0:
        obs = "DÉBITO SSS"
    elif res < 0:
        obs = "POSIBLE ERROR DE TIPEO"
    else:
        obs = "PAGO CORRECTO"
    
    return obs

######################################

def saveHospitalsDetails(tables, totalEntries):
    
    dataind = np.arange(0,totalEntries,1)
    resultFile = pd.DataFrame(index = dataind, columns = ["Fecha de Extraccion",
                                                      "Nombre del Hospital",
                                                      "Expte.",
                                                      "Factura",
                                                      "Foja",
                                                      "RNOS",
                                                      "Denominacion",
                                                      "Imp. Reclamado",
                                                      "Imp. Aprobado",
                                                      "Imp. Depositado",
                                                      "Fecha Deposito",
                                                      "Observaciones"])
    fileIndex = 0
    currentDate = time.strftime("%x")
    currenDateFormatted = currentDate.split("/")[1] + currentDate.split("/")[0] + "20" + currentDate.split("/")[2]
    #currenDateFormatted = "22012018"
    extractionDate = currentDate.split("/")[1] + "/" + currentDate.split("/")[0] + "/" + "20" + currentDate.split("/")[2]
    for element in tables:
        name = element[1]
        table = element[0]
        i = 0
        while(i < len(table)):
            row = table[i]
            
            span = int(row[7][1])
            
            if (span > 1):
                sumaImpApr = 0.0
                for j in range(span):
                    sumaImpApr = sumaImpApr + float(table[i+j][6][0].replace("$","").replace(".","").replace(",","."))
                    
                    resultFile["Fecha de Extraccion"][fileIndex+j] = extractionDate
                    resultFile["Nombre del Hospital"][fileIndex+j] = name
                    resultFile["Nombre del Hospital"][fileIndex+j] = name
                    
                    expteTmp = normalizeExpediente(table[i+j][0][0])
                    resultFile["Expte."][fileIndex + j] = expteTmp
                    
                    facturaTmp = normalizeFactura(table[i+j][1][0])
                    resultFile["Factura"][fileIndex + j] = facturaTmp
                    
                    resultFile["Foja"][fileIndex + j] = table[i+j][2][0]
                    resultFile["RNOS"][fileIndex + j] = table[i+j][3][0]
                    resultFile["Denominacion"][fileIndex + j] = table[i+j][4][0]
                    resultFile["Imp. Reclamado"][fileIndex + j] = table[i+j][5][0]
                    resultFile["Imp. Aprobado"][fileIndex + j] = table[i+j][6][0]
                    resultFile["Imp. Depositado"][fileIndex + j] = table[i+j][7][0]
                    resultFile["Fecha Deposito"][fileIndex + j] = table[i+j][8][0]
                obs = verifyAmount(sumaImpApr, row[7][0])
                resultFile["Observaciones"][fileIndex] = obs
                i = i+span
                fileIndex = fileIndex+span
            else:
                resultFile["Fecha de Extraccion"][fileIndex] = extractionDate
                resultFile["Nombre del Hospital"][fileIndex] = name
                
                expte = normalizeExpediente(row[0][0])
                resultFile["Expte."][fileIndex] = expte
                
                factura = normalizeFactura(row[1][0])
                resultFile["Factura"][fileIndex] = factura
                
                resultFile["Foja"][fileIndex] = row[2][0]
                resultFile["RNOS"][fileIndex] = row[3][0]
                resultFile["Denominacion"][fileIndex] = row[4][0]
                resultFile["Imp. Reclamado"][fileIndex] = row[5][0]
                resultFile["Imp. Aprobado"][fileIndex] = row[6][0]
                if '\n' in row[7][0]:
                    lines = row[7][0].split('\n')
                    sumaImpDep = 0
                    strImp = ''
                    for line in lines:
                        sumaImpDep = sumaImpDep + float(line.replace("$","").replace(".","").replace(",","."))
                        strImp = strImp + line + ' / '
                    resultFile["Imp. Depositado"][fileIndex] = strImp
                    obs = verifyAmount(row[6][0], sumaImpDep)
                else:
                    obs = verifyAmount(row[6][0], row[7][0])
                    resultFile["Imp. Depositado"][fileIndex] = row[7][0]
                    
                resultFile["Fecha Deposito"][fileIndex] = row[8][0].split('\n')[0]
                resultFile["Observaciones"][fileIndex] = obs
                i = i+1
                fileIndex = fileIndex+1
    
    filename = "Datos Hospitales-" + currenDateFormatted + ".csv"
    resultFile.to_csv(filename,sep=";", index = False,encoding="latin-1")
    

######################################

def readConfig():
    
    username = ""
    password = ""
    HPGDList = []
    nameList = []
    readingCodes = False
    
    with open("./sss-auto/config") as conf:
        content = conf.readlines();
    
    content = [x.strip() for x in content]
    
    for line in content:
        if "[USERNAME]" in line:
            username = line.split("=")[1]
        elif "[PASSWORD]" in line:
            password = line.split("=")[1]
        elif readingCodes:
            if "EOF" in line:
                break
            code = line.split("=")[2]
            name = line.split("=")[1]
            HPGDList.append(code)
            nameList.append(name)
        elif "Facturacion" in line:
            readingCodes = True
            
    return [username, password, HPGDList, nameList]

######################################

def begin(strDesde, strHasta):
    
    configs = readConfig()
    
    sssUsername = configs[0]
    sssPassword = configs[1]
    HPGDList = configs[2]
    denominationList = configs[3]
    #prog_label.setText("Iniciando...")
    driver = webdriver.Chrome("./chromedriver.exe")

    driver.get('https://seguro.sssalud.gob.ar/login.php?opc=hpgd')
    #prog_label.setText('Intentando loggearse a: {0}'.format(driver.title))
    doLogin(driver, sssUsername, sssPassword)
    tables = []
    totalEntries = 0
    #progressStep = float(100.0/len(HPGDList))
    #progress = 0
    for i in range(len(HPGDList)):
        #progress = progress + progressStep
        #bar.setValue(int(progress))
        code = HPGDList[i]
        name = denominationList[i]
        
        driver.get('https://seguro.sssalud.gob.ar/indexss.php?opc=bushpgd&s=hpgdfacturasliq')
        
        #prog_label.setText("Buscando información de: " + name)
        table = searchHospitalDetails(driver, code, strDesde, strHasta)
        if (len(table) == 0):
            continue
        else:
            tables.append([table, name])
            totalEntries = totalEntries + len(table)
    #prog_label.setText("Guardando archivo...")
    saveHospitalsDetails(tables, totalEntries)
    #bar.setValue(100)
    #prog_label.setText("Listo!")
        
    driver.close()
