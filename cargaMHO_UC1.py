#Importing Libraries
import pandas
import numpy as np
import time
from time import gmtime, strftime
import os
from selenium import webdriver

#############
### UTILS ###
#############

####################################################################################
####################################################################################

def log(fnCode, file, hospital = None, OS = None, facturas = None, amount = None, filename = None, error = None):

    if (fnCode == "LOGIN"):
        file.write("Loggeandose en: MHO\n")
    elif (fnCode == "F600"):
        file.write("Accediendo a Registro Multiple de Valores a Acreditar...\n")
    elif (fnCode == "PARAMETERS"):
        file.write("Cargando parámetros...\n")
        file.write("\tHospital: {0}\n\tObra Social: {1}\n".format(hospital, OS))
    elif (fnCode == "AMOUNT"):
        file.write("Imputando facturas: {0}\n".format(facturas))
        file.write("Monto a Imputar: {0}\n".format(amount))
    elif (fnCode == "ERROR"):
        file.write("Ha ocurrido un Error: {0}\n".format(error))
    elif (fnCode == "CONFIG"):
        file.write("Leyendo las configuraciones del programa...\n")
    elif (fnCode == "DATA"):
        file.write("Procesando archivo CSV...\n")
    elif (fnCode == "START"):
        file.write("PROGRAMA INICIADO\n")
    elif (fnCode == "END"):
        file.write("PROGRAMA FINALIZADO\n")
    elif (fnCode == "REPORT"):
        file.write("Generando reporte...\n")
    elif (fnCode == "DOWNLOADING"):
        file.write("Reporte generado! Descargando...\n")
    elif (fnCode == "FILE READY"):
        file.write("Listo! Nombre del archivo: {0}\n".format(filename))
    file.write("------------------------------\n")

#############################################################

def validateAmount(monto):
    try:
        print("Antes de pars: "+monto)
        if monto is None:
            monto = "$0"
        print("Despu de pars: "+monto)
        montoRes = ""
        if ("/" in monto):
            montoList = monto.split(" / ")
            sumMontos = 0
            for m in montoList:
                if(m == ""):
                    continue
                m = m.replace("$","").replace(".","").replace(",",".").strip()
                mNumber = float(m)
                sumMontos = sumMontos + mNumber
            montoRes = str(sumMontos)
            montoRes = montoRes.replace(".",",")
        else:
            montoRes = monto.replace("$","").replace(".","").strip()
        if ("," in montoRes):
            if (montoRes.split(",")[1] == "0" or montoRes.split(",")[1] == "00"):
                montoRes = montoRes.split(",")[0]
            elif ("," in montoRes and montoRes[-1]=='0'):
                montoRes = montoRes[:-1]
        return montoRes.strip()
    except Exception as e:
        print("Unexpected Error while validating amount: {0}".format(str(e)))

######################################

def getHospIndex(hospName, hospNamesList):
    try:
        index = hospNamesList.index(hospName)
        return index
    except Exception as e:
        return -1

######################################

def getOSIndex(osRnos, rnosList):
    try:
        index = rnosList.index(osRnos)
        return index
    except Exception as e:
        return -1

######################################

def normalizeFactura(numFactura):
    try:
        if (len(numFactura) > 8):
            numFactura = numFactura[-8:]
        elif (len(numFactura) < 8):
            rest = 8 - len(numFactura)
            ceros = "0"*rest
            numFactura = ceros + numFactura
        return numFactura
    except Exception as e:
        print("Unexpected Error while normalizing factura: {0}".format(str(e)))

###########################################################################################
###########################################################################################

################
### SETTINGS ###
################

###########################################################################################
###########################################################################################

def readConfig(logfile):

    try:

        noFactList = []
        HPGDList = []
        nameList = []
        hospValuesList = []

        RnosList = []
        siglasOS = []
        osValuesList = []

        readingCodes = False
        readingOS = False

        with open("config",encoding="latin1") as conf:
            content = conf.readlines();

        content = [x.strip() for x in content]

        for line in content:
            if readingCodes:
                if "RNOS" in line:
                    readingOS = True
                    readingCodes = False
                    continue;
                fact = line.split("=")[0]
                value = line.split("=")[1]
                name = line.split("=")[2]
                code = line.split("=")[3]
                noFactList.append(fact)
                hospValuesList.append(value)
                HPGDList.append(code)
                nameList.append(name)
            elif "Facturacion" in line:
                readingCodes = True
            elif readingOS:
                if "EOF" in line:
                    break;
                rnos = line.split("=")[2]
                sigla = line.split("=")[1]
                osValue = line.split("=")[0]
                RnosList.append(rnos)
                siglasOS.append(sigla)
                osValuesList.append(osValue)

        hospInfo = [nameList, HPGDList, hospValuesList, noFactList]
        osInfo = [RnosList, siglasOS, osValuesList]

        return [hospInfo, osInfo]
    except KeyboardInterrupt:
        logfile.close()
    except Exception as e:
        print("Unexpected Error: {0}".format(str(e)))
        log("ERROR", logfile, error="Error leyendo las configuraciones: {0}".format(str(e)))
        logfile.close()
        raise RuntimeError("Error leyendo las configuraciones: {0}".format(str(e)))

###############################################################################################
###############################################################################################

#################
### READ DATA ###
#################

###############################################################################################
###############################################################################################

def processCSV(filename, logfile):
    try:
        data = pandas.read_csv(filename, index_col=False, header=0, sep=";",encoding="latin1") #encoding="latin1"
##        print(data.count)
        dfEntries = []
        hospNamesList = list(map(lambda l: str(l), data["HOSPITAL"]))
        uniqueHospNames = list(set(hospNamesList))
        entries = []

        for hospName in uniqueHospNames:
            dfByHosp = data.loc[(data['HOSPITAL'] == hospName)]
            osNamesList = list(map(lambda l: str(l), dfByHosp["Denominacion"]))
            uniqueOsNames = list(set(osNamesList))
            for osName in uniqueOsNames:
                listDfHospOS = []
                dfByHospOS = dfByHosp.loc[(dfByHosp['Denominacion'] == osName)]
                listOfFirsts = []
                dfByHospOS = dfByHospOS.reset_index()
                for i in range(len(dfByHospOS)):
                    if(isinstance(dfByHospOS["Imp. Depositado"][i],str)):
                        listOfFirsts.append(i)
                listOfFirsts.append(len(dfByHospOS))

                for i in range(len(listOfFirsts)-1):
                    minBound = listOfFirsts[i]
                    maxBound = listOfFirsts[i+1]
                    df = dfByHospOS[minBound:maxBound]
                    listDfHospOS.append(df)

                if len(listDfHospOS) == 1:
                    dfEntries.append(listDfHospOS[0])
                else:
                    montos = []
                    facturas = []
                    fecha = None
                    rnos = None
                    fechaGetted = False
                    rnosGetted = False
                    for df in listDfHospOS:
                        df = df.reset_index()

                        if (not fechaGetted):
                            fecha = df["Fecha Deposito"][0]
                            fechaGetted = True
                        if (not rnosGetted):
                            rnos = df["RNOS"][0]
                            rnosGetted = True

                        montoDf = df["Imp. Depositado"][0]
                        montoDfValidated = validateAmount(montoDf)
                        print("-----> MontoDF: ",str(montoDf)," - ",str(montoDfValidated))
                        montos.append(float(montoDfValidated.replace(",",".")))
                        facturas += list(map(lambda l: str(l), df["Factura"]))
                    montoFinal = str(sum(montos)).replace(".",",")
                    montoFinal = validateAmount(montoFinal)
                    dfIndex = np.arange(0,len(facturas),1)
                    dfFinal = pandas.DataFrame(index=dfIndex, columns=["HOSPITAL",
                                                                       "RNOS",
                                                                       "Factura",
                                                                       "Imp. Depositado",
                                                                       "Fecha Deposito"])
                    for i in range(len(facturas)):
                        dfFinal["HOSPITAL"][i] = hospName
                        dfFinal["RNOS"][i] = rnos
                        dfFinal["Factura"][i] = facturas[i]
                        if i==0:
                            dfFinal["Imp. Depositado"][i] = str(montoFinal).replace(".",",")
                            dfFinal["Fecha Deposito"][i] = fecha
                    dfEntries.append(dfFinal)

        for df in dfEntries:
            #indexes = df.index.tolist()
            df = df.reset_index()
            first = 0
            hospName = df["HOSPITAL"][first]
            #print(hospName)
            rnos = str(df["RNOS"][first])
            if(len(rnos) < 6):
                size = len(rnos)
                diff = 6 - len(rnos)
                rnos = ("0"*diff) + rnos
            nosFactura = list(map(lambda l: str(l), df["Factura"]))
            monto = df["Imp. Depositado"][first]
            montoReal = validateAmount(monto)
            fechaDeposito = df["Fecha Deposito"][first]
            item = [hospName, rnos, nosFactura, montoReal, fechaDeposito]
            entries.append(item)
        #for item in entries:
        #   print(item)
        return entries
    except KeyboardInterrupt:
        logfile.close()
    except Exception as e:
        print("Unexpected Error: {0}".format(str(e)))
        log("ERROR", logfile, error="Error mientras se procesaba el archivo de pagos: {0}".format(str(e)))
        logfile.close()
        raise RuntimeError("Error mientras se procesaba el archivo de pagos: {0}".format(str(e)))

#############################################################################################
#############################################################################################

######################
### MHO OPERATIONS ###
######################

###############################################################################################
###############################################################################################

def doLogin (driver, user, password, logfile):
    try:
        userBox = driver.find_element_by_id("txtLoginUsername")
        passBox = driver.find_element_by_id("pwdLoginPassword")
        submit = driver.execute_script('\
            var submitButton = document.querySelectorAll(\'input[value="Iniciar sesión"]\')[0];\
            return submitButton;')

        userBox.send_keys(user)
        passBox.send_keys(password)
        submit.click()
    except KeyboardInterrupt:
        logfile.close()
    except Exception as e:
        print("Unexpected Error while Logging in: {0}".format(str(e)))
        log("ERROR", logfile, error="Error en el inicio de sesion: {0}".format(str(e)))
        logfile.close()
        driver.quit()
        raise RuntimeError("Error en el inicio de sesion: {0}".format(str(e)))

#################################################################

def gotoF600 (driver, payment, paymentsControl, logfile):
    ret = False
    try:
        gestFinButton = driver.find_element_by_id("704")
        gestFinButton.click()
        time.sleep(1)
        f600Button = driver.find_element_by_id("405")
        f600Button.click()
        time.sleep(1)
        driver.execute_script('\
            document.querySelectorAll(".list-title.ng-binding")[2].click()')
        ret = True
        return ret
    except KeyboardInterrupt:
        logfile.close()
        with open("successful", 'w',"latin1") as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w',"latin1") as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]
    except Exception as e:
        print("Unexpected Error F600: {0}".format(str(e)))
        #paymentsControl['failed'].append(payment)
        log("ERROR", logfile, error="Error ingresando a F600: {0}".format(str(e)))
        return ret

#################################################################

def loadParameters (driver, hospCode, hospCodFact, RnosCode, nosFacturas, payment, paymentsControl, logfile):

    ret = False
    input("----HERE")
    fechaHasta = strftime("%d/%m/%Y", gmtime())

    selectHosp = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("option[value=\'' + hospCode + '\']").selected=true;\
document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31024_control").onchange(); '

    desde = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31025_control").value="01/01/2016";\
document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31025_control").onchange(); '

    hasta = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31026_control").value="' + fechaHasta + '";\
document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31026_control").onchange(); '

##    states = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelectorAll("[title=\'Selecciona a Todos\']")[0].click(); '

    selectOS = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31029_control > option[value=\'{0}\']").selected=true;\
document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31029_control").onchange();'.format(RnosCode)

##    clickStates = 'document.querySelector("iframe#fraCentro").contentWindow.document.getElementById("Parametro_31027_button").click();'
    loadingCommand = selectHosp + desde + hasta + states + clickStates

    try:
        driver.execute_script(loadingCommand)
        #time.sleep(1)
        driver.execute_script(clickStates)
        #time.sleep(15)
        
        driver.execute_script(selectOS)
        time.sleep(5)
        checkboxes = driver.execute_script('\
            var checks = document.querySelector("iframe#fraCentro").contentWindow.document.querySelectorAll("input[type=\'checkbox\']");\
            var labels = [];\
            for(i=16; i<checks.length; i++){\
            var labelFact = checks[i].getAttribute(\'label\');\
            labels.push(labelFact);}\
            return labels;')

        indexFact = 0
        listIndexFact = []

        for item in checkboxes:
            noFactura = item.split("-")[0] + item.split("-")[1].strip()
            for no in nosFacturas:
                code = ""
                if(len(hospCodFact) == 1):
                    code = "000"+hospCodFact
                elif(len(hospCodFact) == 2):
                    code = "00"+hospCodFact
                noFormat = code+no
                if (noFormat == noFactura):
                    listIndexFact.append(indexFact)
            indexFact = indexFact + 1

        factToCheck = []
        for item in listIndexFact:
            factToCheck.append(checkboxes[item])

        if (len(factToCheck) == 0):
            log("ERROR", logfile, error="La/s factura/s a imputar no existe/n en el sistema")
            #paymentsControl['failed'].append(payment)
            return False

        listFacturasBox = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#Parametro_31031_button").click();'
        driver.execute_script(listFacturasBox)
        #time.sleep(1)
        for factLabel in factToCheck:
            script = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("input[label=\'{0}\']").click();'.format(factLabel)
            driver.execute_script(script)
            script = 'document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("input[label=\'{0}\']").checked=true;'.format(factLabel)
            driver.execute_script(script)
        #time.sleep(1)
        driver.execute_script(listFacturasBox)
        #time.sleep(5)
        driver.execute_script('document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("#btnEjecutarInforme").click();')
        ret = True
        return ret
    except KeyboardInterrupt:
        logfile.close()
        with open("successful", 'w') as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w') as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]
    except Exception as e:
        print("Unexpected Error loading parameters: {0}".format(str(e)))
        #paymentsControl['failed'].append(payment)
        log("ERROR", logfile, error="Error mientras se cargaban los filtros: {0}".format(str(e)))
        return ret

#######################################################################################################

def loadAmount(driver, monto, fechaDeposito, payment, paymentsControl, logfile):
    ret = False
    try:
        nroRecibo = fechaDeposito.split("/")[2] + fechaDeposito.split("/")[1] + fechaDeposito.split("/")[0]
        windowBefore = driver.window_handles[0]
        driver.execute_script('document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("iframe#fraReporte9484").contentWindow.document.querySelectorAll("#divReporte > p > a")[0].click();')
        time.sleep(2)
        windowAfter = driver.window_handles[1]
        driver.switch_to.window(windowAfter)
        scriptNroRecibo = 'document.querySelector("#txtNumeroRecibo_control").value="{0}";\
    document.querySelector("#txtNumeroRecibo_control").onchange();'.format(nroRecibo)
        scriptNroCheque = 'document.querySelector("#txtNumeroCheque_control").value="{0}";\
    document.querySelector("#txtNumeroCheque_control").onchange();'.format(nroRecibo)
        scriptImporte = 'document.querySelector("#txtImporte_control").value="{0}";\
    document.querySelector("#txtImporte_control").onchange();'.format(monto)
        driver.execute_script(scriptNroRecibo)
        driver.execute_script(scriptNroCheque)
        driver.execute_script(scriptImporte)
        time.sleep(1)
        saveButton = driver.find_element_by_id("toolbartd2_0")
        saveButton.click()
        driver.switch_to.window(windowBefore)
        ret = True
        return ret
    except KeyboardInterrupt:
        logfile.close()
        with open("successful", 'w') as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w') as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]
    except Exception as e:
        print("Unexpected Error loading Amount: {0}".format(str(e)))
        #paymentsControl['failed'].append(payment)
        log("ERROR", logfile, error="Error mientras se cargaba el monto del pago: {0}".format(str(e)))
        return ret

##################################################################################

def genAndSaveReport(driver, fechaDeposito, hospCode, noFact, siglaOS, logFile, payment, paymentsControl):
    ret = False
    try:
        windowBefore = driver.window_handles[0]
        driver.execute_script('document.querySelector("iframe#fraCentro").contentWindow.document.querySelector("iframe#fraReporte9484").contentWindow.document.querySelectorAll("#divReporte > p > a")[1].click();')
        windowAfter = driver.window_handles[1]
        driver.switch_to.window(windowAfter)
        driver.execute_script('function sleep(ms){\
            return new Promise(resolve => setTimeout(resolve, ms));\
            }\
            async function getReport(){\
            for (var i = 0; i<200; i++){\
            el = document.querySelector("iframe#fraReporte9153").contentWindow.document.querySelectorAll("#divReporte > table > tbody > tr > td > p > span > a")[2];\
            if (el) {\
            el.click();\
            break;\
            }\
            await sleep(1000);\
            }\
            }\
            getReport();')

        log("DOWNLOADING", logFile)
        downloadCheckInterval = 2
        fileSize = ""
        downloadComplete = False
        downloadDir = os.environ["userprofile"]+"\\Downloads\\"
        while not downloadComplete:
            time.sleep(downloadCheckInterval)
            for dFile in os.listdir(downloadDir):
                if 'crdownload' in dFile:
                    newSize = os.path.getsize(downloadDir + "\\" + dFile)
                    if (fileSize == newSize):
                        downloadComplete = True
                    else:
                        fileSize = newSize
                    if downloadComplete:
                        depoDate = fechaDeposito.split("/")[0] + '-' + fechaDeposito.split("/")[1] + '-' + fechaDeposito.split("/")[2]
                        _filename = ""
                        if (len(noFact) == 1):
                            _filename = 'SSS {0} {1}-{2} {3}.pdf'.format(depoDate, hospCode, noFact[0], siglaOS)
                        elif (len(noFact) == 2):
                            _filename = 'SSS {0} {1}-{2} {3} {4}.pdf'.format(depoDate, hospCode, noFact[0], noFact[1], siglaOS)
                        else:
                            cantFact = len(noFact)
                            stringFact = str(cantFact) + "F"
                            _filename = 'SSS {0} {1}-{2} {3}.pdf'.format(depoDate, hospCode, stringFact, siglaOS)
                        os.rename(downloadDir + "\\" + dFile, downloadDir + "\\" + _filename)
                        log("FILE READY", logFile, filename = _filename)

        windowReporte = driver.window_handles[2]
        driver.switch_to.window(windowReporte)
        driver.close()
        driver.switch_to.window(windowAfter)
        driver.close()
        driver.switch_to.window(windowBefore)
        ret = True
        return ret
    except KeyboardInterrupt:
        logFile.close()
        with open("successful", 'w') as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w') as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]
    except Exception as e:
        print("Unexpected Error on Report Generation: {0}".format(str(e)))
        #paymentsControl['failed'].append(payment)
        log("ERROR", logFile, error="Error mientras se generaba el reporte: {0}".format(str(e)))
        windowReporte = driver.window_handles[2]
        driver.switch_to.window(windowReporte)
        driver.close()
        driver.switch_to.window(windowAfter)
        driver.close()
        driver.switch_to.window(windowBefore)
        return ret

###########################################################################################################
###########################################################################################################

######################
### MAIN FUNCTIONS ###
######################

###########################################################################################################
###########################################################################################################

def MHOHandler(driver, hospName, hospValue, hospCodFact, codeOS, nosFactura, nosFacturaMinimal, monto, fechaDeposito, siglaOS, logFile, payment, paymentsControl):
    ret = False
    try:
        log("PARAMETERS", logFile, hospital=hospName, OS=siglaOS)
        ret = loadParameters(driver, hospValue, hospCodFact, codeOS, nosFactura, payment, paymentsControl, logFile)
        #time.sleep(20)
        if (ret):
            time.sleep(5)
            stringFacturas = ""
            for i in range(len(nosFactura)):
                if (i < len(nosFactura)-1):
                    stringFacturas = stringFacturas + nosFactura[i] + ", "
                else:
                    stringFacturas = stringFacturas + nosFactura[i]
            log("AMOUNT", logFile, facturas=stringFacturas, amount=monto)
            ret = loadAmount(driver, monto, fechaDeposito, payment, paymentsControl, logFile)
            if (ret):
                time.sleep(3)
                ret = genAndSaveReport(driver, fechaDeposito, hospCodFact, nosFacturaMinimal, siglaOS, logFile, payment, paymentsControl)
                if (ret):
                    paymentsControl['success'].append(payment)
        return ret
    except KeyboardInterrupt:
        logFile.close()
        with open("successful", 'w') as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w') as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]
    except Exception as e:
        print("Unexpected Error MHO HANDLER: {0}".format(str(e)))
        return False

######################################

def processEntry(wd, entry, nameHospitals, hospValues, codFacturacion, rnosList, osValues, siglasOS, logFile, paymentsControl):
    try:
        ret = False
        hospName = entry[0]
        hospIndex = getHospIndex(hospName, nameHospitals)
        if (hospIndex == -1):
            print (hospName)
            log("ERROR", logFile, error="El hospital {0} no existe en el listado".format(hospName))
            return ret

        hospValue = hospValues[hospIndex]
        hospCodFact = codFacturacion[hospIndex]

        osRnos = str(entry[1])
        osIndex = getOSIndex(osRnos, rnosList)
        if (osIndex == -1):
            log("ERROR", logFile, error="La Obra Social no existe en el listado")
            return ret

        osValue = osValues[osIndex]
        osSigla = siglasOS[osIndex]

        nosFactura = []
        for fact in entry[2]:
            fact = normalizeFactura(fact)
            nosFactura.append(fact)

        payment = "{0}|{1}".format(hospCodFact, osSigla)

        nosFacturaMinimal = []
        for fact in nosFactura:
            lastZero = 0
            num = fact
            for character in num:
                if (character != "0"):
                    break;
                else:
                    lastZero = lastZero+1
            num = num[lastZero:]
            nosFacturaMinimal.append(str(num))

        for nfm in nosFacturaMinimal:
            payment = payment+"|"+nfm

        monto = entry[3]
        fechaDeposito = entry[4]

        depoDate = fechaDeposito.split("/")[0] + '-' + fechaDeposito.split("/")[1] + '-' + fechaDeposito.split("/")[2]
        fileDownloaded = ""
        if (len(nosFacturaMinimal) == 1):
            fileDownloaded = 'SSS {0} {1}-{2} {3}.pdf'.format(depoDate, hospCodFact, nosFacturaMinimal[0], osSigla)
        elif (len(nosFacturaMinimal) == 2):
            fileDownloaded = 'SSS {0} {1}-{2} {3} {4}.pdf'.format(depoDate, hospCodFact, nosFacturaMinimal[0], nosFacturaMinimal[1], osSigla)
        else:
            cantFact = len(nosFacturaMinimal)
            stringFact = str(cantFact) + "F"
            fileDownloaded = 'SSS {0} {1}-{2} {3}.pdf'.format(depoDate, hospCodFact, stringFact, osSigla)
        downloadDir = os.environ["userprofile"]+"\\Downloads\\"
        existFile = False
        for dFile in os.listdir(downloadDir):
            if (fileDownloaded in dFile):
                existFile = True
                break

        #print("nombre: {0}, hospValue: {1}, codfact: {2}, osvalue: {3}, monto: {4}".format(hospName, hosp Value, hospCodFact, osValue, monto))
        if ((payment in paymentsControl['success']) or existFile):
            print("El pago ya ha sido procesado")
            log("ERROR", logFile, error="El pago ya ha sido procesado")
            return False
        else:
            log("F600", logFile)
            ret = gotoF600(wd, payment, paymentsControl, logFile)
            time.sleep(5)
            if(ret):
                ret = MHOHandler(wd, hospName, hospValue, hospCodFact, osValue, nosFactura, nosFacturaMinimal, monto, fechaDeposito, osSigla, logFile, payment, paymentsControl)
            return ret
    except KeyboardInterrupt:
        logFile.close()
        with open("successful", 'w') as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w') as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]

######################################

def main(user, pswd, sssCSVFile, paymentsControl):

    try:
        logFile = open("log.txt", 'w')

        log("START", logFile)
        log("CONFIG", logFile)

        configs = readConfig(logFile)
        #Settings
        username = user
        password = pswd
        nameHospitals = configs[0][0]
        HPGDList = configs[0][1]
        hospValues = configs[0][2]
        codFacturacion = configs[0][3]
        rnosList = configs[1][0]
        siglasOS = configs[1][1]
        osValues = configs[1][2]

        #initialURL = "http://saludtest.rhc.gov.ar/mho_app/Login.asp?redir=%2Fmho_app%2FDefault%2Easp#"
        initialURL = "http://salud.cba.gov.ar/mho_fact/Login.asp?redir=%2Fmho_fact%2FDefault%2Easp"

        log("DATA", logFile)
        entries = processCSV(sssCSVFile, logFile)

        options = webdriver.ChromeOptions()
        profile = {"plugins.plugins_list": [{"enabled": False,
                                         "name": "Chrome PDF Viewer"}],
                   "download.extensions_to_open": "",
                   "download.prompt_for_download": False,
                   "profile.default_content_setting_values.automatic_downloads": 1,
                   "safebrowsing.enabled": True,}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--safebrowsing-disable-download-protection')
        wd = webdriver.Chrome("./chromedriver.exe", chrome_options=options)

        wd.get(initialURL)
        log("LOGIN", logFile)
        doLogin(wd, username, password, logFile)

        for entry in entries:
            ret = processEntry(wd, entry, nameHospitals, hospValues, codFacturacion, rnosList, osValues, siglasOS, logFile, paymentsControl)
            if (not ret):
                continue
        wd.quit()
        log("END", logFile)
        logFile.close()
        with open("successful", 'w') as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w') as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]
    except KeyboardInterrupt:
        logFile.close()
        with open("successful", 'w') as succFile:
                [succFile.write(x+'\n') for x in paymentsControl["success"]]
        #with open("failed", 'w') as failFile:
        #   [failFile.write(x+'\n') for x in paymentsControl["failed"]]

#################################
